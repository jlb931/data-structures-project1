import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class NYCStreetTrees
{
	public static void main(String[] args)
	{
//		variable initializations
		File file = null;
		Scanner fileInput = null;
		TreeList treeList = new TreeList();
		
//		Checks to see if there is an arg from the command line.  If there isn't, creates a blank file
		if(args.length>0)
		{
			file = new File(args[0]);
		}
		else
		{
			System.err.println("No file name entered");
			file = new File("");
		}
		
		try
		{
			fileInput = new Scanner(file);
//			Reads a csv file line by line and adds to the list of trees from this file
				do
				{
					try
					{
						ArrayList<String> lineList = splitCSVLine(fileInput.nextLine());
//						Getting required variables from lineList to make a tree
						int id = Integer.parseInt(lineList.get(0));
						int diam = Integer.parseInt(lineList.get(3));
						String status = lineList.get(6);
						String health = lineList.get(7);
						String spc = lineList.get(9);
						int zip = Integer.parseInt(lineList.get(25));
						String boro = lineList.get(29);
						double x = Double.parseDouble(lineList.get(39));
						double y = Double.parseDouble(lineList.get(40));
//						Makes a tree using information acquired above
						treeList.add(new Tree(id, diam, status, health, spc, zip, boro, x, y));
					}
					catch (IllegalArgumentException e)
					{
						
					}
				} while(fileInput.hasNextLine());
			fileInput.close();
			
//			Gets species name input to search with
			System.out.println("Enter the tree species to learn more about it (enter \"quit\" to stop):");
			Scanner input = new Scanner(System.in);
			String species = input.nextLine();
			
//			Searches for a species and prints out info about that species
			while(!species.equalsIgnoreCase("quit"))
			{
//				Identifies all of species name in the treeList
				ArrayList<String> matchingSpeciesList = treeList.getMatchingSpecies(species);
				if(matchingSpeciesList.size()==0)
				{
					System.out.println("There are no records of " + species + " on NYC streets");
				}
				else
				{
//					Prints out all matching species
					System.out.println("All matching species: ");
					for(int i = 0; i<matchingSpeciesList.size(); i++)
					{
						System.out.print("\n" + matchingSpeciesList.get(i));
					}
//					Finds all counts for trees of a certain species in a certain borough
					int countByTreeSpeciesBoroughManhattan = treeList.getCountByTreeSpeciesBorough(species, "Manhattan");
					int countByTreeSpeciesBoroughBronx = treeList.getCountByTreeSpeciesBorough(species, "Bronx");
					int countByTreeSpeciesBoroughBrooklyn = treeList.getCountByTreeSpeciesBorough(species, "Brooklyn");
					int countByTreeSpeciesBoroughQueens = treeList.getCountByTreeSpeciesBorough(species, "Queens");
					int countByTreeSpeciesBoroughStaten = treeList.getCountByTreeSpeciesBorough(species, "Staten Island");
					int countTotalBySpecies = treeList.getCountByTreeSpecies(species);
					
//					Finds all counts for trees in a certain borough
					int countByBoroughManhattan = treeList.getCountByBorough("Manhattan");
					int countByBoroughBronx = treeList.getCountByBorough("Bronx");
					int countByBoroughBrooklyn = treeList.getCountByBorough("Brooklyn");
					int countByBoroughQueens = treeList.getCountByBorough("Queens");
					int countByBoroughStaten = treeList.getCountByBorough("Staten Island");
					int countTotal = treeList.getTotalNumberOfTrees();
					
//					Finds the percent of a species of tree in a borough and total
					double percentManhattan = ((double) countByTreeSpeciesBoroughManhattan/(double) countByBoroughManhattan)*100;
					double percentBronx = ((double) countByTreeSpeciesBoroughBronx/(double) countByBoroughBronx)*100;
					double percentBrooklyn = ((double) countByTreeSpeciesBoroughBrooklyn/(double) countByBoroughBrooklyn)*100; 
					double percentQueens = ((double) countByTreeSpeciesBoroughQueens/(double) countByBoroughQueens)*100;
					double percentStaten = ((double) countByTreeSpeciesBoroughStaten/(double) countByBoroughStaten)*100;
					double percentTotal = ((double) countTotalBySpecies/(double) countTotal)*100;
					
//					Prints out formatted results
					System.out.println("\nPopularity in the city: ");
					System.out.printf("\t%-10s\t%d(%d)\t%10f%%", "NYC", countTotalBySpecies, countTotal, percentTotal);
					System.out.printf("\n\t%-10s\t%d(%d)\t%10f%%", "Manhattan", countByTreeSpeciesBoroughManhattan, countByBoroughManhattan, percentManhattan);
					System.out.printf("\n\t%-10s\t%d(%d)\t%10f%%", "Bronx", countByTreeSpeciesBoroughBronx, countByBoroughBronx, percentBronx);
					System.out.printf("\n\t%-10s\t%d(%d)\t%10f%%", "Brooklyn", countByTreeSpeciesBoroughBrooklyn, countByBoroughBrooklyn, percentBrooklyn);
					System.out.printf("\n\t%-10s\t%d(%d)\t%10f%%", "Queens", countByTreeSpeciesBoroughQueens, countByBoroughQueens, percentQueens);
					System.out.printf("\n\t%-10s\t%d(%d)\t%10f%%", "Staten Island", countByTreeSpeciesBoroughStaten, countByBoroughStaten, percentStaten);
				}
				System.out.println("\n\nEnter the tree species to learn more about it (enter \"quit\" to stop):");
				species = input.nextLine();
			}
			input.close();
		} 
		catch (FileNotFoundException e)
		{
			System.err.println("Usage Error: the program expects file name as an argument.");
		}
			
	}

	/**
	 * Splits the given line of a CSV file according to commas and double quotes
	 * (double quotes are used to surround multi-word entries that may contain
	 * commas).
	 * 
	 * @param textLine
	 *            line of text to be parsed
	 * @return an ArrayList object containing all individual entries/tokens
	 *         found on the line.
	 */
	public static ArrayList<String> splitCSVLine(String textLine)
	{
		ArrayList<String> entries = new ArrayList<String>();
		int lineLength = textLine.length();
		StringBuffer nextWord = new StringBuffer();
		char nextChar;
		boolean insideQuotes = false;
		boolean insideEntry = false;

		// iterate over all characters in the textLine
		for (int i = 0; i < lineLength; i++)
		{
			nextChar = textLine.charAt(i);

			// handle smart quotes as well as regular quotes
			if (nextChar == '"' || nextChar == '\u201C' || nextChar == '\u201D')
			{
				// change insideQuotes flag when nextChar is a quote
				if (insideQuotes)
				{
					insideQuotes = false;
					insideEntry = false;
				} else
				{
					insideQuotes = true;
					insideEntry = true;
				}
			} else if (Character.isWhitespace(nextChar))
			{
				if (insideQuotes || insideEntry)
				{
					// add it to the current entry
					nextWord.append(nextChar);
				} else
				{ // skip all spaces between entries
					continue;
				}
			} else if (nextChar == ',')
			{
				if (insideQuotes) // comma inside an entry
					nextWord.append(nextChar);
				else
				{ // end of entry found
					insideEntry = false;
					entries.add(nextWord.toString());
					nextWord = new StringBuffer();
				}
			} else
			{
				// add all other characters to the nextWord
				nextWord.append(nextChar);
				insideEntry = true;
			}

		}
		// add the last word (assuming not empty)
		// trim the white space before adding to the list
		if (!nextWord.toString().equals(""))
		{
			entries.add(nextWord.toString().trim());
		}

		return entries;
	}

}
